package lab.inoue.refactoringtest.handlers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ExecTest{
	final static String EXEC_ANT = "C:/UserSoft/apache-ant-1.9.6-bin/apache-ant-1.9.6/bin/ant.bat";
	final static String XERCES_PATH = "C:/Users/threepipes_s/Work/laboratory/Downloads/xerces_old/trunk/build.xml";
	final static String LOG_PATH = "C:/Users/threepipes_s/Work/laboratory/Downloads/ant_log_new_origin.txt";
	final static String[] CMD_XERCES = {EXEC_ANT, "-f", XERCES_PATH, "test"};
	private String preResult;
	private String compare = "No comparable";
	public void exec(){
		// apache-ant	総テスト数:235	時間(おおよそ):9分
//		(new ExecProcess(CMD_ANT, "ant_log.txt", "ANT")).start();
		// xerces	総テスト数:79	時間:50s
//		(new ExecProcess(CMD_XERCES, "xerces_log.txt", "XERCES")).start();
//		parseFile(LOG_PATH);
//		String result = execProcess(new String[]{"cmd.exe", "/c", "echo|build.bat", "test"}, Data.testDir);
		String[] cmd = {"cmd.exe", "/c", "echo|build.bat", "test"};
		String dir = Data.testDir;
		ExecProcess ep = new ExecProcess(cmd, dir, this);
		ep.start();
	}
	
	public void setResult(String result){
		if(preResult != null){
			if(preResult.equals(result)){
				compare = "build test: Test passed.";
			}else{
				compare = "build test: Test failed.";
			}
			System.out.println(compare);
		}else{
			System.out.println("Pretest finished.");
		}
		preResult = result;
	}
}

class ExecProcess extends Thread{
	ProcessBuilder pb;
	ExecTest test;
	public ExecProcess(String[] cmd, String dir, ExecTest etest) {
		pb = new ProcessBuilder(cmd);
		if(dir != null) pb.directory(new File(dir));
		test = etest;
	}
	
	@Override
	public void run() {
		String res = execProcess();
		res = parseString(res);
		test.setResult(res);
	}

	private String execProcess(){
		try {
			Process pc;
			pc = pb.start();
			BufferedReader br = new BufferedReader(
					new InputStreamReader(pc.getInputStream()));
			String line;
			StringBuilder sb = new StringBuilder();
			while((line = br.readLine()) != null){
				sb.append(line);
				sb.append("\n");
			}
			pc.waitFor();
			br.close();
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String parseString(String result){
		String[] line = result.split("\n");
		StringBuilder sb = new StringBuilder();
		int status = 0;
		for(int i=0; i<line.length; i++){
			if(line[i].indexOf("[javac]")!=-1 || line[i].indexOf("[echo]")!=-1){
				sb.append(line[i]);
				sb.append('\n');
				if(line[i].indexOf("[echo]")!=-1 && line[i].indexOf("Running")!=-1){
					status++;
				}
			}else if(status%2==0 && line[i].indexOf("[java]")!=-1){
				sb.append(line[i]);
				sb.append('\n');
			}
		}
		return sb.toString();
	}
}