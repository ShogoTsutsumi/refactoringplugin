package lab.inoue.refactoringtest.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.internal.corext.callhierarchy.CallHierarchy;
import org.eclipse.jdt.internal.corext.callhierarchy.MethodWrapper;

public class RefactoringCandidate {
	RefApplyer refApplyer;
	public RefactoringCandidate(RefApplyer ra) {
		refApplyer = ra;
	}
	
	public List<Applyer> setRefactoringCandidate(IType ocu, IType ncu){
//		IType ocu = getIType(Data.oldProject, oldClassName);
//		IType ncu = getIType(Data.newProject, newClassName);
		ICompilationUnit ocf = ocu.getCompilationUnit();
		ICompilationUnit ncf = ncu.getCompilationUnit();
//		setExtractMethodCandidate(ocu, ncu);
		List<Applyer> list = new ArrayList<>();
		list.addAll(setPullUpFieldCandidate(getSuperclass(ocu, Data.packageName, Data.oldProject), ocf, ncf));
		list.addAll(setPullUpMethodCandidate(ocu, ocf, ncf));
		list.addAll(setExtractMethodCandidate(ocf, ncf));
//		list.addAll(setDeletedMethodCandidate(ocu,
//				getSuperclass(ocu, Data.packageName, Data.oldProject).getCompilationUnit()));
		return list;
	}

	private int[] getLinePosition(String src){
		List<Integer> list = new ArrayList<>();
		for(int i=src.indexOf('\n'); i!=-1; i=src.indexOf('\n', i+1)){
			list.add(i);
		}
		int[] res = new int[list.size()];
		for(int i=0; i<res.length; i++) res[i] = list.get(i);
		return res;
	}
	
	// extract_methodの抽出候補の元となる行
	private int[][] getLineOffsets(IMethod method, int[] linePos){
		int s = 0;
		int t = 0;
		ISourceRange sourceRange;
		try {
			sourceRange = method.getSourceRange();
			int offset = sourceRange.getOffset();
			int len = sourceRange.getLength();
			for(int i=0; i<linePos.length; i++){
				if(linePos[i]>=offset) s = i;
				else if(linePos[i]>offset+len){
					t = i;
					break;
				}
			}
			int sz = t-s;
			int[][] res = new int[sz*(sz+1)/2][2];
			int cnt = 0;
			for(int i=s; i<t; i++){
				for(int j=i+1; j<t; j++){
					res[cnt][0] = linePos[i];
					res[cnt][1] = linePos[j]-1;
					cnt++;
				}
			}
			return res;
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}
	class Pair{
		int s, t;
		Pair(int s, int t){
			this.s = s;
			this.t = t;
		}
	}
	List<Pair> tmpList = new ArrayList<>();
	/**
	 * 新規メソッド内ブロックと呼び出し側旧メソッドの処理が似ていると判断する閾値
	 */
	final double threshold = 0.75;
	public List<Applyer> setExtractMethodCandidate(ICompilationUnit ocu, ICompilationUnit ncu){
		// 新規メソッド
		MyMethod[] nMethod = getCreatedMethods(ocu, ncu);
		IMethod[] newMethod = getIMethods(ncu, nMethod);
		IMethod[] searchMethod = getCallersOf(newMethod);
		HashMap<IMethod, List<Range>> ranges = getCallPosition(searchMethod, newMethod);
		// range の前と後の文の位置を探す -> 断念，全探索することに
		HashMap<IMethod, List<IMethod>> oldMethods = getCorresponds(ranges, ocu);
		List<Applyer> list = new ArrayList<>();
		try{
			for(Entry<IMethod, List<IMethod>> e: oldMethods.entrySet()){
				IMethod createdMethod = e.getKey();
				ISourceRange range = createdMethod.getSourceRange();
				ISourceRange docRange = createdMethod.getJavadocRange();
				ISourceRange nameRange = createdMethod.getNameRange();
				String source = createdMethod.getSource();
				String noDocSource = source.substring(docRange.getLength(), source.length());
				String cBody = noDocSource.substring(noDocSource.indexOf('{')+1, noDocSource.lastIndexOf('}'));
				int ln = (int) cBody.chars().filter(c->c=='\n').count();
				int minLine = Math.max(1, (int)(ln*0.8));
				int maxLine = (int)(ln*1.2);
//				System.err.println("about method: "+createdMethod.getElementName());
//				System.err.println(cBody);
//				System.err.println();
				for(IMethod m: e.getValue()){
//					char[] source = range.contain.getCompilationUnit().getSource().toCharArray();
					// target: createdMethodのbodyと相対的な編集距離が近くなるようなsourceの切り取り方を候補の一つとする．
					// 候補は行単位
					// 最終的に，切り取る範囲を(ソースファイル内の位置として)得たい
					String src = m.getSource();
					String mBody = src.substring(src.indexOf('{')+1, src.lastIndexOf('}'));
					String[] lines = mBody.split("\n");
					int[] paren = new int[lines.length];
					for(int i=0; i<lines.length; i++){
						for(int j=0; j<lines[i].length(); j++){
							if(lines[i].charAt(j)=='{'){
								paren[i]++;
							}else if(lines[i].charAt(j)=='}'){
								paren[i]--;
							}
						}
						lines[i] += '\n';
//						if(i>0) paren[i] += paren[i-1];
					}
//					System.err.println(mBody);
//					System.err.println();
					// 全探索: 行数^2(定数倍軽め) * トークン数^2 の処理なので激重
					int startSize = Math.min(minLine, lines.length);
					StringBuilder sb = new StringBuilder();
//					String dbg = null;
					double minDist = 1;
					int minStartLine = -1;
					int minSize = -1;
					out:for(int startLine=0; startLine+startSize<=lines.length; startLine++){
						sb.delete(0, sb.length());
						int idx = startLine;
						int sz = 0;
						int parenSum = 0;
						for(int i=0; i<startSize-1; i++, sz++){
							parenSum += paren[idx];
							if(parenSum<0) continue out;
							sb.append(lines[idx++]);
						}
						for(; idx<lines.length && sz<maxLine; sz++){
							parenSum += paren[idx];
							sb.append(lines[idx++]);
							if(parenSum<0) continue out;
							if(parenSum != 0)
								continue;
							String s = sb.toString();
							double score = RefApplyer.getDist(cBody, s);
							if(score<minDist){
								minDist = score;
								minStartLine = startLine;
								minSize = sz;
//								dbg = s;
							}
						}
					}
					// このときのminStartLineとminSizeが切り抜き行を示す
					// これをソースコード上の位置に変換する
					if(minDist > threshold) continue;
					
//					System.out.println("Start debug:");
//					System.out.print(dbg);
//					System.out.println("End debug:");
					
					ISourceRange r = m.getSourceRange();
					final int methodEnd = r.getOffset()+r.getLength();
					final int methodStart = getMemberStartOffset(m, methodEnd);
					tmpList.add(new Pair(methodStart, methodEnd));
					int start = methodStart;
					for(int i=0; i<minStartLine; i++){
						start += lines[i].length();
					}
					int end = start;
					for(int i=minStartLine; i<=minStartLine+minSize; i++){
						end += lines[i].length();
					}
					list.add(new ExtractMethodApplyer(ocu, start, end, createdMethod, refApplyer));
				}
			}
		}catch(JavaModelException e){
			e.printStackTrace();
		}
		return list;
		/*
		List<MyMethod> mList = getMethods(ocu);
		MyMethod[] oMethod = mList.toArray(new MyMethod[mList.size()]);
		// 共通メソッド
		MyMethod[] cMethod = getCommonMethods(oMethod, ncu);
		*/
	}
	private int getMemberStartOffset(IMethod method, int methodEnd) throws JavaModelException{
		ISourceRange sr = method.getSourceRange();
		ISourceRange dr = method.getJavadocRange();
		if(dr==null){
			return sr.getOffset()+method.getSource().indexOf('{')+1;
		}
		String noDocSource = method.getSource().substring(dr.getLength(), sr.getLength());
		return sr.getOffset()+dr.getLength() + noDocSource.indexOf('{') + 1;
	}
	
	/**
	 * ICompilationUnitのソースから，Rangeに含まれるメソッドに対応するメソッドを取得する．
	 */
	private HashMap<IMethod, List<IMethod>> getCorresponds(HashMap<IMethod, List<Range>> ranges, ICompilationUnit cu){
		HashMap<IMethod, List<IMethod>> ans = new HashMap<>();
		for(Entry<IMethod, List<Range>> e: ranges.entrySet()){
			IMethod calledMethod = e.getKey();
			List<Range> range = e.getValue();
			IMethod[] meth = new IMethod[range.size()];
			for(int i=0; i<range.size(); i++) meth[i] = range.get(i).contain;
			List<IMethod> list = getCorrespondMethods(meth, cu);
			ans.put(calledMethod, list);
		}
		return ans;
	}
	/**
	 * ICompilationUnitのソースから，methodsに対応する(名前と引数の型が同じ)メソッドのリストを取得する．
	 */
	private List<IMethod> getCorrespondMethods(IMethod[] meth, ICompilationUnit cu){
		IJavaElement elements[];
		List<IMethod> list = new ArrayList<>();
		HashSet<String> map = new HashSet<>();
		// ハッシュマップでメソッドを検索できるように，IMethodをUniqueな文字列に変更
		for(IMethod m: meth){
			map.add(getIMethodString(m));
		}
		try {
			elements = cu.getChildren();
			for (IJavaElement javaElement : elements) {
				if (javaElement.getElementType() == IJavaElement.TYPE) {
					IType type = (IType) javaElement;
					IMethod[] methods = type.getMethods();
					for (IMethod method : methods) {
						String str = getIMethodString(method);
						if(map.contains(str)){
							list.add(method);
						}
//						if(method.getElementName().equals(meth.)
//								&& equals(method.getParameterTypes(), meth.TYPES))
//							return method;
					}
				}
			}
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return list;
	}
	
	private String getIMethodString(IMethod meth){
		String str = meth.getElementName()+":";
		for(String p: meth.getParameterTypes()){
			str += p+",";
		}
		return str;
	}
	
	private HashMap<IMethod, List<Range>> getCallPosition(IMethod[] method, IMethod[] called){
		HashMap<String, IMethod> nameMap = new HashMap<>();
		for(IMethod m: called) nameMap.put(m.getElementName(), m);
		CallPositionGetter visitor = new CallPositionGetter(nameMap);
		try{
			for(IMethod m: method){
				visitor.setSearchingMethod(m);
				ASTParser psr = ASTParser.newParser(AST.JLS8);
				psr.setSource(m.getCompilationUnit());
				ISourceRange r = m.getSourceRange();
				psr.setSourceRange(r.getOffset(), r.getLength());
				ASTNode node = psr.createAST(new NullProgressMonitor());
				node.accept(visitor);
			}
		}catch(JavaModelException e){
			e.printStackTrace();
		}
		return visitor.getPosition();
	}
	
	class CallPositionGetter extends ASTVisitor{
		private HashMap<IMethod, List<Range>> pos;
		private HashMap<String, IMethod> map;
		private IMethod searching;
		public CallPositionGetter(HashMap<String, IMethod> map) {
			pos = new HashMap<>();
			for(IMethod m: map.values()) pos.put(m, new ArrayList<>());
			this.map = map;
		}
		public void setSearchingMethod(IMethod m){
			searching = m;
		}
		public boolean visit(MethodInvocation node) {
			String name = node.getName().getIdentifier();
			if(!map.containsKey(name)) return true;
			List<Range> p = pos.get(map.get(name));
			int offset = node.getStartPosition();
			int length = node.getLength();
			p.add(new Range(offset, length, searching));
			return true;
		}
		public HashMap<IMethod, List<Range>> getPosition(){
			return pos;
		}
	}
	
	class Range{
		int offset, length;
		IMethod contain;
		Range(int os, int len, IMethod m){
			offset = os;
			length = len;
			contain = m;
		}
	}
	
	private IMethod[] getCallersOf(IMethod[] method){
		if(method.length==0) return new IMethod[]{};
		HashSet<IMethod> set = new HashSet<>();
		for(IMethod m: method){
			set.addAll(getCallersOf(m));
		}
		IMethod[] res = set.toArray(new IMethod[set.size()]);
		if(res.length==0){
			res = getMyCallersOf(method);
		}
		return res;
	}
	
	private IMethod[] getMyCallersOf(IMethod[] method){
		HashMap<String, IMethod> map = new HashMap<>();
		for(IMethod m: method){
			map.put(m.getElementName(), m);
		}
		List<IMethod> res = new ArrayList<>();
		ICompilationUnit cu = method[0].getCompilationUnit();
		IMethod[] allMethod = getIMethods(cu, toArray(getMethods(cu)));
		try{
			for(IMethod m: allMethod){
				ContainingCallChecker visitor = new ContainingCallChecker(map, m.getElementName());
				ASTParser psr = ASTParser.newParser(AST.JLS8);
				psr.setSource(m.getCompilationUnit());
				ISourceRange r = m.getSourceRange();
				psr.setSourceRange(r.getOffset(), r.getLength());
				ASTNode node = psr.createAST(new NullProgressMonitor());
				node.accept(visitor);
				if(visitor.found) res.add(m);
			}
		}catch(JavaModelException e){
			e.printStackTrace();
		}
		return res.toArray(new IMethod[res.size()]);
	}
	
	private MyMethod[] toArray(List<MyMethod> m){
		return m.toArray(new MyMethod[m.size()]);
	}
	
	class ContainingCallChecker extends ASTVisitor{
		HashMap<String, IMethod> map;
		boolean found = false;
		String methodName;
		public ContainingCallChecker(HashMap<String, IMethod> map, String name) {
			methodName = name;
			this.map = map;
		}
		public boolean visit(MethodDeclaration node) {
			String name = node.getName().getIdentifier();
			return name.equals(methodName);
		}
		@Override
		public boolean visit(MethodInvocation node) {
			String name = node.getName().getIdentifier();
			if(!map.containsKey(name)) return true;
			found = true;
			return false;
		}
	}
	
	private List<Applyer> setDeletedMethodCandidate(IType otype, ICompilationUnit cu){
		List<MyMethod> mlist = getMethods(cu);
		MyMethod[] method = mlist.toArray(new MyMethod[mlist.size()]);
		// 子クラスに含まれるメソッドを対象とする
		method = getCommonMethods(method
				, otype.getCompilationUnit());
		IMethod[] im = getIMethods(cu, method);
		List<Applyer> res = new ArrayList<>();
		for(IMethod m: im) res.add(new RemoveApplyer(m, refApplyer));
		return res;
	}
	
	private List<Applyer> setPullUpMethodCandidate(IType otype, ICompilationUnit ocu, ICompilationUnit ncu){
		MyMethod[] method = getDeletedMethods(ocu, ncu);
		IType parent = getSuperclass(otype, Data.packageName, Data.oldProject);
		if(parent==null) return new ArrayList<>();
		MyMethod[] cmethod = getCommonMethods(method
				, parent.getCompilationUnit());
		MyMethod[] ncmethod = getNotCommonMethods(method
				, parent.getCompilationUnit());
		IMethod[] im = getIMethods(ocu, ncmethod);
		List<Applyer> res = new ArrayList<>();
		// 親クラスに含まれないメソッドを対象とする
		for(IMethod m: im) res.add(new PullUpMethodApplyer(m, parent, refApplyer));
		im = getIMethods(ocu, cmethod);
		IMethod[] pim = getIMethods(parent.getCompilationUnit(), cmethod);
		if(im.length != pim.length){
			System.err.println("Error! wrong length");
			return res;
		}
		for(int i=0; i<im.length; i++){
			res.add(new DeleteAndPullUpMethodApplyer(im[i], pim[i], refApplyer));
		}
			
		return res;
//		for(MyMethod m: method) System.out.println(m);
		
	}
	
	private HashSet<IMethod> getCallersOf(IMethod m){
		CallHierarchy callH = CallHierarchy.getDefault();
		IMember[] members = {m};
		MethodWrapper[] mWrappers = callH.getCallerRoots(members);
		HashSet<IMethod> callers = new HashSet<>();
		for(MethodWrapper mw: mWrappers){
			MethodWrapper[] mw2 = mw.getCalls(new NullProgressMonitor());
			HashSet<IMethod> tmp = getIMethods(mw2);
			callers.addAll(tmp);
		}
		return callers;
	}
	
	private HashSet<IMethod> getIMethods(MethodWrapper[] mWrappers){
		HashSet<IMethod> c = new HashSet<>();
		for(MethodWrapper m: mWrappers){
			IMethod im = getIMethodFromMethodWrapper(m);
			if(im != null){
				c.add(im);
			}
		}
		return c;
	}
	
	private IMethod getIMethodFromMethodWrapper(MethodWrapper m){
		try{
			IMember im = m.getMember();
			if(im.getElementType() == IJavaElement.METHOD){
				return (IMethod)m.getMember();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	private List<Applyer> setPullUpFieldCandidate(IType par, ICompilationUnit ocu, ICompilationUnit ncu){
		MyField[] field = getDeletedFields(ocu, ncu);
		IField[] ifld = getIFields(ocu, field);
//		for(MyField f: field) System.out.println(f);
		List<Applyer> res = new ArrayList<>();
		if(par==null) return res;
		for(IField f: ifld) res.add(new PullUpFieldApplyer(f, par, refApplyer));
		return res;
	}
	

	private List<MyMethod> getMethods(ICompilationUnit cu){
		ASTParser psr = ASTParser.newParser(AST.JLS8);
		psr.setSource(cu);
		ASTNode node = psr.createAST(new NullProgressMonitor());
		MethodFinder mf = new MethodFinder();
		node.accept(mf);
		return mf.getMethods();
	}
	
	private MyMethod[] getCreatedMethods(ICompilationUnit from, ICompilationUnit to){
		ASTParser fromPsr = ASTParser.newParser(AST.JLS8);
		fromPsr.setSource(from);
		ASTNode fromNode = fromPsr.createAST(new NullProgressMonitor());
		ASTParser toPsr = ASTParser.newParser(AST.JLS8);
		toPsr.setSource(to);
		ASTNode toNode = toPsr.createAST(new NullProgressMonitor());
		return getCreatedMethods(fromNode, toNode);
	}
	
	private MyMethod[] getCommonMethods(MyMethod[] method, ICompilationUnit cu){
		HashSet<MyMethod> set = new HashSet<>();
		set.addAll(getMethods(cu));
		List<MyMethod> res = new ArrayList<>();
		for(MyMethod m: method){
			if(set.contains(m)) res.add(m);
		}
		return res.toArray(new MyMethod[res.size()]);
	}
	
	private MyMethod[] getNotCommonMethods(MyMethod[] method, ICompilationUnit cu){
		HashSet<MyMethod> set = new HashSet<>();
		set.addAll(getMethods(cu));
		List<MyMethod> res = new ArrayList<>();
		for(MyMethod m: method){
			if(!set.contains(m)) res.add(m);
		}
		return res.toArray(new MyMethod[res.size()]);
	}
	
	private MyMethod[] getDeletedMethods(ICompilationUnit from, ICompilationUnit to){
		return getCreatedMethods(to, from);
	}
	
	private MyMethod[] getCreatedMethods(ASTNode from, ASTNode to){
		return getDeletedMethods(to, from);
	}
	
	private MyMethod[] getDeletedMethods(ASTNode from, ASTNode to){
		HashSet<MyMethod> set = new HashSet<>();
		MethodFinder mf = new MethodFinder();
		to.accept(mf);
		set.addAll(mf.getMethods());
		
		mf = new MethodFinder();
		from.accept(mf);
		List<MyMethod> meth = mf.getMethods();
		List<MyMethod> res = new ArrayList<>();
		for(MyMethod m: meth){
			if(!set.contains(m)) res.add(m);
		}
		return res.toArray(new MyMethod[res.size()]);
	}
	
	private MyField[] getDeletedFields(ICompilationUnit from, ICompilationUnit to){
		return getCreatedFields(to, from);
	}
	
	private MyField[] getCreatedFields(ICompilationUnit from, ICompilationUnit to){
		ASTParser fromPsr = ASTParser.newParser(AST.JLS8);
		fromPsr.setSource(from);
		ASTNode fromNode = fromPsr.createAST(new NullProgressMonitor());
		ASTParser toPsr = ASTParser.newParser(AST.JLS8);
		toPsr.setSource(to);
		ASTNode toNode = toPsr.createAST(new NullProgressMonitor());
		return getCreatedFields(fromNode, toNode);
	}
	
	private MyField[] getCreatedFields(ASTNode from, ASTNode to){
		return getDeletedFields(to, from);
	}
	
	private MyField[] getDeletedFields(ASTNode from, ASTNode to){
		HashSet<MyField> set = new HashSet<>();
		FieldFinder mf = new FieldFinder();
		to.accept(mf);
		set.addAll(mf.getFields());
		
		mf = new FieldFinder();
		from.accept(mf);
		List<MyField> meth = mf.getFields();
		List<MyField> res = new ArrayList<>();
		for(MyField m: meth){
			if(!set.contains(m)) res.add(m);
		}
		return res.toArray(new MyField[res.size()]);
	}
	
	private IMethod getIMethod(MyMethod meth, ICompilationUnit cu){
		IJavaElement elements[];
		try {
			elements = cu.getChildren();
			for (IJavaElement javaElement : elements) {
				if (javaElement.getElementType() == IJavaElement.TYPE) {
					IType type = (IType) javaElement;
					IMethod[] methods = type.getMethods();
					for (IMethod method : methods) {
						if(method.getElementName().equals(meth.name)
								&& equals(method.getParameterTypes(), meth.TYPES))
							return method;
					}
				}
			}
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}
	
	private boolean equals(String[] a1, String[] a2){
		if(a1.length != a2.length) return false;
		for(int i=0; i<a1.length; i++){
			if(!a1[i].equals(a2[i])) return false;
		}
		return true;
	}
	

	private MyMethod getNewMethod(ICompilationUnit cunit, MyMethod method){
		ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(cunit);
		ASTNode node = parser.createAST(new NullProgressMonitor());
		OneMethodFinder mf = new OneMethodFinder(method);
		node.accept(mf);
		return mf.getMethod();
	}

	
	public static IType getSuperclass(IType cls, String packageName, String projectName){
		try {
			String name = cls.getSuperclassName();
			if(name == null) return null;
			return getIType(projectName, packageName+"."+name);
//			IJavaElement[] elements = cu.getChildren();
//			for (IJavaElement javaElement : elements) {
//				// ↓型だったらば、ITypeにキャスト。
//				if (javaElement.getElementType() == IJavaElement.TYPE) {
//					IType type = (IType) javaElement;
//					if(type.getElementName().equals(name)) return type;
//				}
//			}
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}
	
	public static IType getIType(String projectName, String fullClassName){
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject(projectName);
		IJavaProject ijp = JavaCore.create(project);
		if(ijp == null) return null;
		// castできるか不明
		try {
			return ijp.findType(fullClassName);
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}
	
	public static ICompilationUnit getCompilationUnit(String projectName, String fullClassName){
		IType type = getIType(projectName, fullClassName);
		if(type == null) return null;
		return type.getCompilationUnit();
	}
	
	
	private IMethod[] getIMethods(ICompilationUnit cu, MyMethod[] meth){
		HashSet<String> set = new HashSet<String>();
		for(MyMethod m: meth) set.add(m.name);
		List<IMethod> list = new ArrayList<>();
		try {
			IJavaElement[] elements = cu.getChildren();
			for (IJavaElement javaElement : elements) {
				// ↓型だったらば、ITypeにキャスト。
				if (javaElement.getElementType() == IJavaElement.TYPE) {
					IType type = (IType) javaElement;
					// メソッド一覧を取得。
					IMethod[] methods = type.getMethods();
					// IMethod method = methods[methods.length - 1];
					for (IMethod method : methods) {
//						System.out.println(method.getElementName());
						if(set.contains(method.getElementName())){
							list.add(method);
						}
					}
				}
			}
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return list.toArray(new IMethod[list.size()]);
	}
	
	private IField[] getIFields(ICompilationUnit cu, MyField[] fld){
		HashSet<String> set = new HashSet<String>();
		for(MyField f: fld) set.add(f.name);
		List<IField> list = new ArrayList<>();
		try {
			IJavaElement[] elements = cu.getChildren();
			for (IJavaElement javaElement : elements) {
				// ↓型だったらば、ITypeにキャスト。
				if (javaElement.getElementType() == IJavaElement.TYPE) {
					IType type = (IType) javaElement;
					// メソッド一覧を取得。
					IField[] fields = type.getFields();
					// IMethod method = methods[methods.length - 1];
					for (IField field: fields) {
//						System.out.println(method.getElementName());
						if(set.contains(field.getElementName())){
							list.add(field);
						}
					}
				}
			}
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return list.toArray(new IField[list.size()]);
	}
	
	public static IMethod getIMethod(ICompilationUnit cu, String methodName){
		try {
			IJavaElement[] elements = cu.getChildren();
			for (IJavaElement javaElement : elements) {
				// ↓型だったらば、ITypeにキャスト。
				if (javaElement.getElementType() == IJavaElement.TYPE) {
					IType type = (IType) javaElement;
					// メソッド一覧を取得。
					IMethod[] methods = type.getMethods();
					// IMethod method = methods[methods.length - 1];
					for (IMethod method : methods) {
//						System.out.println(method.getElementName());
						if(method.getElementName().equals(methodName)){
							return method;
						}
					}
				}
			}
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}
	
	private IField getIField(ICompilationUnit cu, String fieldName){
		try {
			IJavaElement[] elements = cu.getChildren();
			for (IJavaElement javaElement : elements) {
				if (javaElement.getElementType() == IJavaElement.TYPE) {
					IType type = (IType) javaElement;
					IField[] fields = type.getFields();
					// IMethod method = methods[methods.length - 1];
					for (IField f : fields) {
//						System.out.println(method.getElementName());
						if(f.getElementName().equals(fieldName)){
							return f;
						}
					}
				}
			}
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}
}
