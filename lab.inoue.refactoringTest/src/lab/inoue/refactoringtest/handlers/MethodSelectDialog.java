package lab.inoue.refactoringtest.handlers;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class MethodSelectDialog extends Dialog{
	MyMethod[] methods;
	public MethodSelectDialog(Shell parent, MyMethod[] methods) {
		super(parent);
		this.methods = methods;
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Select method");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite cp = (Composite)super.createDialogArea(parent);
		Combo cb = new Combo(cp, SWT.SCROLL_LINE | SWT.READ_ONLY);
		cb.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selected = ((Combo)e.widget).getSelectionIndex();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		cb.select(0);
		for(MyMethod m: methods) cb.add(m.toString());
		return cp;
	}
	
	private int selected;
	public int getSelected(){
		return selected;
	}
}
