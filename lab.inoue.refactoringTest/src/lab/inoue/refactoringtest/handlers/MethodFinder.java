package lab.inoue.refactoringtest.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;

public class MethodFinder extends ASTVisitor{
	private List<MyMethod> methods;
	public MethodFinder() {
		methods = new ArrayList<>();
	}
	public List<MyMethod> getMethods(){
		return methods;
	}
	@Override
	public boolean visit(MethodDeclaration method) {
		if(method.isConstructor() || method.getBody()==null) return true;
		int modifire = method.getModifiers();
		String name = method.getName().getIdentifier();
		String type = method.getReturnType2().toString();
		List list = method.parameters();
		String[] body = method.getBody().toString().split("\n");
		methods.add(new MyMethod(modifire, type, name, list, body));
		return true;
	}
	/*
	@Override
	public void preVisit(ASTNode node) {
		if(node instanceof MethodDeclaration){
			MethodDeclaration method = (MethodDeclaration)node;
			if(method.isConstructor()) return;
			int modifire = method.getModifiers();
			String name = method.getName().getIdentifier();
			String type = method.getReturnType2().toString();
			List list = method.parameters();
			String[] body = method.getBody().toString().split("\n");
			methods.add(new MyMethod(modifire, type, name, list, body));
//			System.out.printf("可視性等\t=%s\n", method.modifiers());
//			System.out.printf("戻り型\t=%s\n", method.getReturnType2());
//			System.out.printf("メソッド名\t=%s\n", method.getName().getIdentifier());
//			System.out.printf("引数 \t=%s\n", method.parameters());
		}
	}
	*/
}

class OneMethodFinder extends ASTVisitor{
	private MyMethod oldMethod;
	private MyMethod found;
	public OneMethodFinder(MyMethod m){
		oldMethod = m;
	}
	public MyMethod getMethod(){
		return found;
	}
	@Override
	public void preVisit(ASTNode node) {
		if(node instanceof MethodDeclaration){
			MethodDeclaration method = (MethodDeclaration)node;
			if(method.isConstructor() || found!=null) return;
			int modifire = method.getModifiers();
			String name = method.getName().getIdentifier();
			String type = method.getReturnType2().toString();
			List list = method.parameters();
			String[] body = method.getBody().toString().split("\n");
			MyMethod m = new MyMethod(modifire, type, name, list, body);
			if(oldMethod.equals(m)) found = m;
		}
	}
}
