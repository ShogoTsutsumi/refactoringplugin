package lab.inoue.refactoringtest.handlers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;

import org.eclipse.compare.rangedifferencer.RangeDifference;
import org.eclipse.compare.rangedifferencer.RangeDifferencer;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class SampleHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	MyMethod[] methods;
	public SampleHandler() {
	}

	/*
	private MyMethod selectMethod(IWorkbenchWindow workbenchWindow, ICompilationUnit cunit){
//		MyMethod[] methods = getMethods(getSourcePath());
		if(methods==null){
			methods = getCommonMethods(getSourcePath(), cunit);
		}
		MethodSelectDialog dialog = new MethodSelectDialog(workbenchWindow.getShell(), methods);
		if(dialog.open()==IDialogConstants.CANCEL_ID) return null;
		return methods[dialog.getSelected()];
	}

	private MyMethod[] getCommonMethods(String path, ICompilationUnit cunit){
		char[] src = getSource(path).toCharArray();
		if(src==null) return new MyMethod[0];
		ASTParser fromPsr = ASTParser.newParser(AST.JLS8);
		fromPsr.setSource(cunit);
		ASTNode fromNode = fromPsr.createAST(new NullProgressMonitor());
		ASTParser toPsr = ASTParser.newParser(AST.JLS8);
		toPsr.setSource(src);
		ASTNode toNode = toPsr.createAST(new NullProgressMonitor());
		return getCreatedMethods(fromNode, toNode);
//		return new MyMethod[]{
//			new MyMethod(MyMethod.PRIVATE, "void", "newMethod", new ArrayList()),
//			new MyMethod(MyMethod.PUBLIC, "void", "oldMethod", new ArrayList()),
//			new MyMethod(MyMethod.PROTECTED, "void", "proMethod", new ArrayList()),
//			new MyMethod(MyMethod.NONE, "void", "noneMethod", new ArrayList()),
//		};
	}
	 */
	
	private void astVisit(ICompilationUnit cu, ASTVisitor v){
		ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(cu);
		ASTNode node = parser.createAST(new NullProgressMonitor());
		node.accept(v);
	}

	private void printASTNodes(ICompilationUnit cu){
		/*
		try {
			IType[] type = cu.getAllTypes();
			List<IMember> member = new ArrayList<>();
			for(IType t: type){
				IField[] ifld = t.getFields();
				for(IField f: ifld) member.add(f);
				IMethod[] im = t.getMethods();
				for(IMethod m: im) member.add(m);
			}
			for(IMember im: member){
				System.out.println(im.getSource());
				System.out.println();
			}
//			new TokenSplitter(cu.getSource().toCharArray()).getTokens().stream().forEach(System.out::println);
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		/**/
		
		try {
			ASTParser parser = ASTParser.newParser(AST.JLS8);
			parser.setSource(cu);
			String src = cu.getSource();
			ASTNode node = parser.createAST(new NullProgressMonitor());
			CompilationUnit unit = ((CompilationUnit)node);
			unit.recordModifications();
			//		MethodFinder mf = new MethodFinder();
			Printer pr = new Printer();
			node.accept(pr);
			IDocument doc = new Document(src);
			TextEdit edit = unit.rewrite(doc, null);
			edit.apply(doc);
			src = doc.get();
			File file = new File(Data.workSpacePath+cu.getPath().toString());
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
			out.print(src);
			out.close();
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (MalformedTreeException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (BadLocationException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}
	class Printer extends ASTVisitor{
//		@Override
//		public void preVisit(ASTNode node) {
//			System.out.println(node.toString());
//		}
		@Override
		public boolean visit(MethodDeclaration node) {
			if(node.getName().getIdentifier().equals("testMethod"))
				node.delete();
			return false;
		}
	}

	String getSource(String path){
		try {
			StringBuilder sb = new StringBuilder();
			BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream(path)));
			int c;
			while((c=reader.read())!=-1){
				sb.append((char)c);
			}
			reader.close();
			return sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

//	final static String sourcePath = "C:/Users/threepipes_s/Work/laboratory/xerces_work/source_new.java";
//	private String getSourcePath(){
//		return sourcePath;
//	}
	
	private IProject getIProject(String projectName){
		return ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
	}

	public Object execute(ExecutionEvent event) throws ExecutionException {
		// ワークベンチの取得．ここからワークベンチ内のほとんどの情報が取得できる
		IWorkbench workbench = PlatformUI.getWorkbench();
		// アクティブになっているウィンドウ(フォーカスがあっているウィンドウ)を取得
		IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();

		// IComplicationUnitの取得
		IEditorPart editor = workbenchWindow.getActivePage().getActiveEditor();
		IJavaElement javaElement = JavaUI.getEditorInputJavaElement(editor.getEditorInput());
		if (javaElement.getElementType() != IJavaElement.COMPILATION_UNIT){
			// should throw exception
			return null;
		}
		ICompilationUnit cunit = (ICompilationUnit)javaElement;
		//					IType type = cunit.findPrimaryType();
		//		final Shell shell = getShell();
//		pullUpMethodRefactoring(getCompilationUnit("TestGit"), "testMethod", shell);
//		new ExecTest().exec();
		
//		astVisit(cunit, new ASTTester());
		/* EXPERIMENT
		final String oldProjectName = "PluginTest";
		final String newProjectName = "PluginTest2";
		final String oldClassName = "org.test.PluginTest";
		final String newClassName = "org.test.PluginTest";
		final String packageName = "org.apache.xerces.parsers.";
//		IType oldType = RefactoringCandidate.getIType(oldProjectName, oldClassName);
//		IType newType = RefactoringCandidate.getIType(newProjectName, newClassName);
		IType oldType = RefactoringCandidate.getIType("XercesForPlugin", packageName+"DTDConfiguration");
		IType newType = RefactoringCandidate.getIType("XercesForPlugin2", packageName+"DTDConfiguration");
		ICompilationUnit oldCU = oldType.getCompilationUnit();
		ICompilationUnit newCU = newType.getCompilationUnit();
		
		System.out.println(oldCU);
		System.out.println(newCU);
		RefApplyer test = new RefApplyer(getIProject("PluginTest"));
		RefactoringCandidate cand = new RefactoringCandidate(test);
		List<Applyer> list = cand.setExtractMethodCandidate(oldCU, newCU);
		ExtractMethodApplyer ap = (ExtractMethodApplyer)list.get(0);
		try {
			String source = oldCU.getSource();
			System.out.println(source.substring(ap.start, ap.start+ap.len));
			char[] src = source.toCharArray();
			StringBuilder sb = new StringBuilder();
			for(int i=ap.start; i<ap.start+ap.len; i++){
				sb.append(src[i]);
				src[i] = '*';
			}
			System.out.println();
			System.out.println(String.valueOf(src));
			System.out.println(sb.toString());
//			List<RefactoringCandidate.Pair> li = cand.tmpList;
//			for(RefactoringCandidate.Pair p: li){
//				sb.setLength(0);
//				System.out.println("START PRINTING METHOD");
//				for(int i=p.s; i<p.t; i++){
//					sb.append(src[i]);
//				}
//				System.out.println(sb.toString());
//			}
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		ap.apply();
		
		if(true) return null;
		*/
		RefApplyer ra = new RefApplyer(getIProject(Data.oldProject));
		ra.search(Data.packageName+"."+Data.className, Data.packageName+"."+Data.className);
		/**/
//		ra.test();
//		ra.removeMethod(RefactoringCandidate.getIMethod(RefactoringCandidate.getCompilationUnit("TestGit", "Child"), "testMethod"));
//		printASTNodes(RefactoringCandidate.getCompilationUnit("TestGit", "Child"));
		//		setRefactoringCandidate("org.apache.xerces.dom.DocumentImpl"
		//				, "org.apache.xerces.dom.DocumentImpl");
		//		RefApplyer ra = new RefApplyer(prj);
		//		ra.test();
		return null;

		//		MyMethod meth = selectMethod(workbenchWindow, cunit);
		//		if(meth==null) return null;
		//		refactoring(cunit, offset, textSelection.getLength(), meth);

		//		MyMethod newMeth = getNewMethod(cunit, meth);
		//		String diff = getMethodDiff(newMeth.body, meth.body);
		//		MessageConsole console = new MessageConsole("Diff", null);
		//		IConsoleManager consoleManager = ConsolePlugin.getDefault().getConsoleManager();
		//		consoleManager.addConsoles(new IConsole[]{console});
		//		MessageConsoleStream consoleStream = console.newMessageStream();
		//		consoleStream.println(diff);

	}



	String getMethodDiff(String[] m1, String[] m2){
		RangeDifference[] diff = RangeDifferencer.findRanges(
				new MethodDiffer(m1), new MethodDiffer(m2));
		StringBuilder sb = new StringBuilder();
		for(RangeDifference d: diff){
			if(d.kind()==RangeDifference.NOCHANGE){
				sb.append(setStrWithChar(m1, d.leftStart(), d.leftEnd(), -1));
			}else{
				sb.append(setStrWithChar(m1, d.leftStart(), d.leftEnd(), '-'));
				sb.append(setStrWithChar(m2, d.rightStart(), d.rightEnd(), '+'));
			}
		}
		return sb.toString();
	}

	String setStrWithChar(String[] m, int l, int r, int top){
		StringBuilder sb = new StringBuilder();
		for(int i=l; i<r; i++){
			if(top>=0) sb.append((char)top);
			sb.append("\t"+m[i]+"\n");
		}
		return sb.toString();
	}
}
