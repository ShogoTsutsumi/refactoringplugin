package lab.inoue.refactoringtest.handlers;

import java.util.ArrayList;
import java.util.List;

public class TokenSplitter {
	private List<String> result;
	public TokenSplitter(char[] src) {
		result = split(src);
	}
	private List<String> split(char[] s){
		List<String> res = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		boolean preChar = false;
		int commentState = 0;
		for(int i=0; i<s.length; i++){
			if(1<=commentState && commentState<=4){
				if(commentState==2) commentState++;
				else if(commentState==4) commentState = 0;
				else if(commentState==3 && s[i]=='*'
						&& i<s.length-1 && s[i+1]=='/') commentState = 4;
				else if(commentState==1 && s[i]=='\n') commentState = 0;
			}else if(Character.isWhitespace(s[i])){
				if(preChar){
					res.add(sb.toString());
					sb.setLength(0);
					preChar = false;
				}
			}else if(Character.isLowerCase(s[i])
					|| Character.isUpperCase(s[i])
					|| Character.isDigit(s[i])
					|| s[i] == '_' || s[i] == '.'){
				preChar = true;
				sb.append(s[i]);
			}else{
				if(s[i] == '/' && i<s.length-1){
					if(s[i+1] == '/'){
						commentState = 1;
						continue;
					}else if(s[i+1] == '*'){
						commentState = 2;
						continue;
					}
				}
				if(preChar){
					res.add(sb.toString());
					sb.setLength(0);
					preChar = false;
				}
				res.add(String.valueOf(s[i]));
			}
		}
		return res;
	}
	public List<String> getTokens(){
		return result;
	}
	public String[] getTokenArray(){
		return result.toArray(new String[result.size()]);
	}
}
