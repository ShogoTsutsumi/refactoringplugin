package lab.inoue.refactoringtest.handlers;

import org.eclipse.compare.rangedifferencer.IRangeComparator;

public class MethodDiffer implements IRangeComparator{
	String[] line;
	public MethodDiffer(String[] line) {
		this.line = line;
	}
	
	@Override
	public int getRangeCount() {
		return line.length;
	}
	
	@Override
	public boolean rangesEqual(int thisIndex, IRangeComparator other, int otherIndex) {
		return line[thisIndex].equals(((MethodDiffer)other).line[otherIndex]);
	}
	
	@Override
	public boolean skipRangeComparison(int length, int maxLength, IRangeComparator other) {
		return false;
	}
}
