package lab.inoue.refactoringtest.handlers;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;

public class ASTTester extends ASTVisitor{
	@Override
	public boolean visit(SimpleName node) {
		return true;
	}
	public boolean visit(MethodInvocation node) {
		System.out.println(node.getName().getIdentifier());
		int offset = node.getStartPosition();
		System.out.println(offset);
		int len = node.getLength();
		System.out.println(len);
		return true;
	}
}
