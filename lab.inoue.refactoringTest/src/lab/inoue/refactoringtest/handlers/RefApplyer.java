package lab.inoue.refactoringtest.handlers;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IImportDeclaration;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageDeclaration;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.ToolFactory;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.formatter.CodeFormatter;
import org.eclipse.jdt.internal.corext.refactoring.code.ExtractMethodRefactoring;
import org.eclipse.jdt.internal.corext.refactoring.structure.PullUpRefactoringProcessor;
import org.eclipse.jdt.internal.ui.preferences.JavaPreferencesSettings;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CheckConditionsOperation;
import org.eclipse.ltk.core.refactoring.CreateChangeOperation;
import org.eclipse.ltk.core.refactoring.PerformChangeOperation;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.ui.PlatformUI;

public class RefApplyer {
	class ProjectState implements Comparable<ProjectState>{
		String branch;
		String key;
		double value;
		ProjectState par;
		Applyer ap;
		int hash;
		ProjectState(String branch, String key, double value, ProjectState parent, Applyer ap){
			this.branch = branch;
			this.key = key;
			this.value = value;
			par = parent;
			this.ap = ap;
		}
		public void printApplyer(){
			if(ap==null) return;
			par.printApplyer();
			System.out.println(ap);
		}
		public void setHash(ICompilationUnit[] cus){
			try{
				int hash = 0;
				for(int i=0; i<cus.length; i++){
					hash ^= cus[i].getSource().hashCode();
				}
				this.hash = hash;
			}catch(JavaModelException e){
				e.printStackTrace();
			}
		}
		@Override
		public int hashCode() {
			return hash;
		}
		@Override
		public boolean equals(Object obj) {
			ProjectState ps = (ProjectState)obj;
			return ps.hash==hash && ps.value==value;
		}
		@Override
		public int compareTo(ProjectState o) {
			return Double.compare(value, o.value);
		}
		@Override
		public String toString() {
			return "[ branch:"+branch+", value:"+value+" ]";
		}
	}
	
	IProject prj;
	public RefApplyer(IProject p) {
		keySet = new HashSet<>();
		rand = new Random(0);
		prj = p;
	}
	

	HashSet<String> keySet;
	Random rand;
	final static int keyLen = 8;
	private String createKey(){
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<keyLen; i++){
			sb.append((char)('a'+rand.nextInt(26)));
		}
		return sb.toString();
	}
	
	public void test(){
//		GitUtil git = new GitUtil(prj);
//		String branch = "base";
//		git.branchAndCommit(branch, "no comment");
		ICompilationUnit cu = RefactoringCandidate.getCompilationUnit("TestGit", "Child");
		IMethod im = RefactoringCandidate.getIMethod(cu, "testMethod");
		pullUpMethodRefactoring(im, RefactoringCandidate.getIType("TestGit", "Test"));
//		String secb = "newbranch";
//		git.branchAndCommit(secb, "second");
//		git.checkout(branch);
//		removeMethod(im);
	}
	
	public void refresh(){
		try {
			prj.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	private void setComilationUnits(IType oldType, IType newType
			, ICompilationUnit oldCU, ICompilationUnit newCU
			, ICompilationUnit oldParCU, ICompilationUnit newParCU,
			String oldClassNameFull, String newClassNameFull){
		oldType = RefactoringCandidate.getIType(Data.oldProject, oldClassNameFull);
		newType = RefactoringCandidate.getIType(Data.newProject, newClassNameFull);
		oldCU = oldType.getCompilationUnit();
		newCU = newType.getCompilationUnit();
		if(oldParCU==null) return;
		oldParCU = RefactoringCandidate.getSuperclass(oldType, Data.packageName, Data.oldProject).getCompilationUnit();
		newParCU = RefactoringCandidate.getSuperclass(newType, Data.packageName, Data.newProject).getCompilationUnit();
	}
	
	public void search(String oldClassNameFull, String newClassNameFull){
		ExecTest etest = new ExecTest();
		etest.exec();
		IType oldType = RefactoringCandidate.getIType(Data.oldProject, oldClassNameFull);
		IType newType = RefactoringCandidate.getIType(Data.newProject, newClassNameFull);
		ICompilationUnit oldCU = oldType.getCompilationUnit();
		ICompilationUnit newCU = newType.getCompilationUnit();
		IType oldPar = RefactoringCandidate.getSuperclass(oldType, Data.packageName, Data.oldProject);
		IType newPar = RefactoringCandidate.getSuperclass(oldType, Data.packageName, Data.newProject);
		ICompilationUnit oldParCU = oldPar!=null?oldPar.getCompilationUnit():null;
		ICompilationUnit newParCU = newPar!=null?newPar.getCompilationUnit():null;
		ICompilationUnit[] oldCUs = oldParCU!=null?new ICompilationUnit[]{oldCU, oldParCU}
										: new ICompilationUnit[]{oldCU};
		ICompilationUnit[] newCUs = newParCU!=null?new ICompilationUnit[]{newCU, newParCU}
										: new ICompilationUnit[]{newCU};
		RefactoringCandidate rc = new RefactoringCandidate(this);
		GitUtil git = new GitUtil(Data.addFiles, prj);
		String branch = "base";
		String key = git.branchAndCommit(branch, "start");
		Queue<ProjectState> qu = new PriorityQueue<>();
		double baseValue = getValue(oldCUs, newCUs);
		qu.add(new ProjectState(branch, key, baseValue, null, null));
		ProjectState bestPrj = qu.peek();
		HashSet<ProjectState> used = new HashSet<>();
		long starttime = System.currentTimeMillis();
		while(!qu.isEmpty() && System.currentTimeMillis()-starttime<600000){
			ProjectState ps = qu.poll();
			if(used.contains(ps)){
				System.out.println("Already done.");
				continue;
			}
			used.add(ps);
			System.out.println("Parent"+ps);
			git.checkout(ps.branch, ps.key);
			setComilationUnits(oldType, newType, oldCU, newCU
					, oldParCU, newParCU, oldClassNameFull, newClassNameFull);
			List<Applyer> applyer = rc.setRefactoringCandidate(oldType, newType);
			System.out.println("APPLYER SIZE:"+applyer.size());
			for(Applyer ap: applyer){
				System.out.println("Ref:"+ap);
				git.checkout(ps.branch, ps.key);
				setComilationUnits(oldType, newType, oldCU, newCU
						, oldParCU, newParCU, oldClassNameFull, newClassNameFull);
				try{
					ap.apply();
				}catch(Exception e){
					System.err.println("Exception!!");
					System.err.println(e);
					git.reset();
					continue;
				}
				double value = getValue(oldCUs, newCUs);
//				if(value>baseValue*2) continue;
				branch = createKey();
				key = git.branchAndCommit(branch, ""+value);
				ProjectState newps = new ProjectState(branch, key, value, ps, ap);
				newps.setHash(oldCUs);
				if(newps.compareTo(bestPrj)<0) bestPrj = newps;
				qu.add(newps);
			}
		}
		bestPrj.printApplyer();
		System.out.println(bestPrj.value);
		git.checkout(bestPrj.branch);
		setComilationUnits(oldType, newType, oldCU, newCU
				, oldParCU, newParCU, oldClassNameFull, newClassNameFull);
		for(int i=0; i<oldCUs.length; i++){
			String src = changeSource(oldCUs[i], newCUs[i]);
			String name = oldCUs[i].getElementName();
			writeResult(Data.OutputDir+"/"+name, src);
		}
		System.out.println("Testing ...");
		etest.exec();
	}
	
	private void writeResult(String filename, String result){
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(filename));
			out.write(result);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String changeSource(ICompilationUnit oldCU, ICompilationUnit newCU){
		List<String[]> list = getSrcForCompare(oldCU, newCU);
		int len = list.size();
		String[] s1 = new String[len];
		String[] s2 = new String[len];
		for(int i=0; i<len; i++){
			String[] v = list.get(i);
			s1[i] = v[0];
			s2[i] = v[1];
		}
		String impStr = getImportSrcForCompare(oldCU, newCU);
		System.out.println("diff about: "+oldCU.getElementName());

		addedMethod = 0;
		addedToken = 0;
		deletedMethod = 0;
		deletedToken = 0;
		String changedSrc = changeStr(s1, s2);
		StringBuilder baseSrc = new StringBuilder(getMemberDeletedSource(oldCU));
		System.out.println("Added member: "+addedMethod);
		System.out.println("Deleted member: "+deletedMethod);
		System.out.println("Added token: "+addedToken);
		System.out.println("Deleted token: "+deletedToken);
		baseSrc.insert(0, impStr);
		baseSrc.insert(baseSrc.indexOf("{")+1, changedSrc);
		return format(baseSrc.toString().trim());
	}
	
	private String getImportSrcForCompare(ICompilationUnit oldCU, ICompilationUnit newCU){
		try {
			StringBuilder src = new StringBuilder();
			IPackageDeclaration[] oldPack = oldCU.getPackageDeclarations();
			IImportDeclaration[] oldImp = oldCU.getImports();
			for(IPackageDeclaration ip: oldPack) src.append(ip.getSource());
			for(IImportDeclaration ii: oldImp) src.append(ii.getSource());
			String oldSrc = src.toString();
			src.setLength(0);
			IPackageDeclaration[] newPack = newCU.getPackageDeclarations();
			IImportDeclaration[] newImp = newCU.getImports();
			for(IPackageDeclaration ip: newPack) src.append(ip.getSource());
			for(IImportDeclaration ii: newImp) src.append(ii.getSource());
			String newSrc = src.toString();
			return changeStr(new String[]{oldSrc}, new String[]{newSrc});
		} catch (JavaModelException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * oldとnewそれぞれから同名のメンバ変数・関数を取り出し，
	 * 対応メンバごとにString[0](old)，String[1](new)に格納
	 * それらをリストにして返す．
	 */
	private List<String[]> getSrcForCompare(ICompilationUnit oldCU, ICompilationUnit newCU){
		List<String[]> list = new ArrayList<>();
		HashMap<String, IMember> lonly = new HashMap<>();
		try{
			HashMap<String, IMember> map = new HashMap<>();
			for(IMember m: getMembers(oldCU))
				map.put(getShortName(m), m);
			for(IMember m: getMembers(newCU)){
				String sn = getShortName(m);
				if(map.containsKey(sn)){
					list.add(new String[]{
							map.get(sn).getSource(),
							m.getSource()
					});
					map.remove(sn);
				}else{
//					list.add(new String[]{
//							"", m.getSource()
//					});
					lonly.put(m.getElementName(), m);
				}
			}
			for(IMember m: map.values()){
				String name = m.getElementName();
				if(lonly.containsKey(name)){
					list.add(new String[]{
						m.getSource(),
						lonly.get(name).getSource(),
					});
					lonly.remove(name);
				}else list.add(new String[]{m.getSource(), ""});
			}
			for(IMember m: lonly.values()){
				list.add(new String[]{"", m.getSource()});
			}
		}catch(JavaModelException e){
			e.printStackTrace();
		}
		return list;
	}
	
	private String getMemberDeletedSource(ICompilationUnit cu){
		try {
			ASTParser parser = ASTParser.newParser(AST.JLS8);
			String src = cu.getSource();
			parser.setSource(src.toCharArray());
			ASTNode node = parser.createAST(new NullProgressMonitor());
			src = node.toString();

			parser.setSource(src.toCharArray());
			node = parser.createAST(new NullProgressMonitor());
			CompilationUnit unit = ((CompilationUnit)node);
			unit.recordModifications();
			AllMemberDeleter pr = new AllMemberDeleter(getTopLevelClasses(cu));
			node.accept(pr);
			IDocument doc = new Document(src);
			TextEdit edit = unit.rewrite(doc, null);
			edit.apply(doc);
			src = doc.get();
			return src;
		} catch (MalformedTreeException e) {
			e.printStackTrace();
		} catch (BadLocationException e) {
			e.printStackTrace();
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		return null;
	}
	private String[] getTopLevelClasses(ICompilationUnit cu){
		try {
			IType[] type = cu.getTypes();
			String[] s = new String[type.length];
			for(int i=0; i<s.length; i++){
				s[i] = type[i].getElementName();
			}
			return s;
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		return null;
	}
	class AllMemberDeleter extends ASTVisitor{
		HashSet<String> cls;
		public AllMemberDeleter(String[] topCls) {
			cls = new HashSet<>();
			for(String s: topCls) cls.add(s);
		}
		@Override
		public boolean visit(TypeDeclaration node) {
			if(!cls.contains(node.getName().getIdentifier())){
				node.delete();
				return false;
			}
			return super.visit(node);
		}
		@Override
		public boolean visit(MethodDeclaration node) {
			node.delete();
			return super.visit(node);
		}
		@Override
		public boolean visit(FieldDeclaration node) {
			node.delete();
			return super.visit(node);
		}
	}
	/**
	 * 対応メンバごとに編集距離をとり，
	 * それに応じた差分値を返す．
	 */
	private double getValue(ICompilationUnit[] oldCU, ICompilationUnit[] newCU){
		List<String[]> list = new ArrayList<>();
		for(int i=0; i<oldCU.length; i++){
			list.addAll(getSrcForCompare(oldCU[i], newCU[i]));
		}
		int len = list.size();
		String[] s1 = new String[len];
		String[] s2 = new String[len];
		for(int i=0; i<len; i++){
			String[] v = list.get(i);
			s1[i] = v[0];
			s2[i] = v[1];
		}
		return getDist(s1, s2);
	}
	
	private String getShortName(IMember m){
		if(m.getElementType() == IJavaElement.METHOD){
			return getShortName((IMethod)m);
		}else if(m.getElementType() == IJavaElement.FIELD){
			return getShortName((IField)m);
		}else{
			return m.getElementName();
		}
	}
	
	private String getShortName(IMethod m){
		StringBuilder sb = new StringBuilder();
		sb.append(m.getElementName()+"(");
		for(String s: m.getParameterTypes())
			sb.append(s+",");
		sb.append(")");
		return sb.toString();
	}
	
	private String getShortName(IField f){
		return f.getElementName();
	}
	/*
	private boolean equals(IMember m1, IMember m2){
		if(m1.getElementType() != m2.getElementType()) return false;
		if(m1.getElementType() == IJavaElement.METHOD){
			return equals((IMethod)m1, (IMethod)m2);
		}else{
			return equals((IField)m1, (IField)m2);
		}
	}
	
	private boolean equals(IMethod m1, IMethod m2){
		if(!m1.getElementName().equals(m2.getElementName())) return false;
		String[] p1 = m1.getParameterTypes();
		String[] p2 = m2.getParameterTypes();
		if(p1.length != p2.length) return false;
		for(int i=0; i<p1.length; i++){
			if(!p1[i].equals(p2[i])) return false;
		}
		return true;
	}
	
	private boolean equals(IField f1, IField f2){
		return f1.getElementName().equals(f2.getElementName());
	}
	*/
	private List<IMember> getMembers(ICompilationUnit cu){
		try {
			IType[] type = cu.getTypes();
			List<IMember> member = new ArrayList<>();
			for(IType t: type){
				IField[] ifld = t.getFields();
				for(IField f: ifld) member.add(f);
				IMethod[] im = t.getMethods();
				for(IMethod m: im) member.add(m);
				IType[] itp = t.getTypes();
				for(IType tp: itp) member.add(tp);
			}
			return member;
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		return null;
	}

	private double getDist(String[] s1, String[] s2){
		int sumToken = 0;
		int dist = 0;
		for(int i=0; i<s1.length; i++){
			String[] from = new TokenSplitter(s1[i].toCharArray()).getTokenArray();
			String[] to = new TokenSplitter(s2[i].toCharArray()).getTokenArray();
			if(from.length==0 || to.length==0){
				dist += from.length+to.length;
			}else{
				LevenshteinDistance<String> ld = new LevenshteinDistance<String>(from, to, 1, 1, 2);
				dist += ld.getDist();
			}
			sumToken += from.length+to.length;
		}
		return (double)dist/sumToken;
	}
	
	public static double getDist(String s1, String s2){
		int dist = 0;
		String[] from = new TokenSplitter(s1.toCharArray()).getTokenArray();
		String[] to = new TokenSplitter(s2.toCharArray()).getTokenArray();
		if(from.length==0 || to.length==0){
			dist = from.length+to.length;
		}else{
			LevenshteinDistance<String> ld = new LevenshteinDistance<String>(from, to, 1, 1, 2);
			dist = ld.getDist();
		}
		int sumToken = from.length+to.length;
		return (double)dist/sumToken;
	}
	
	int addedMethod, deletedMethod;
	int addedToken, deletedToken;
	private String changeStr(String[] s1, String[] s2){
		StringBuilder res = new StringBuilder();
		for(int i=0; i<s1.length; i++){
			StringBuilder sb = new StringBuilder();
			String[] from = new TokenSplitter(s1[i].toCharArray()).getTokenArray();
			String[] to = new TokenSplitter(s2[i].toCharArray()).getTokenArray();
			if(from.length==0){
				// 新たにtoが加えられる
				to[0] = "\n/*<added definition>*/\n"+to[0];
				for(int j=0; j<to.length; j++){
					sb.append(to[j]);
					sb.append(" ");
				}
				addedMethod++;
				addedToken += to.length;
			}else if(to.length==0){
				// fromが削除
				from[0] = "\n/*<deleted definition>\n"+from[0];
				from[from.length-1] += "\n */";
				for(int j=0; j<from.length; j++){
					sb.append(from[j]);
					sb.append(" ");
				}
				deletedMethod++;
				deletedToken += from.length;
			}else{
				LevenshteinDistance<String> ld = new LevenshteinDistance<String>(from, to, 1, 1, 2);
				int del = 0, add = 0;
				for(LevenshteinDistance<String>.Operation op: ld.getOperations()){
					if(op.type==LevenshteinDistance.DEL){
						from[op.idx] = "/*<d> " + from[op.idx] + "*/";
//						from[op.idx] = "";
						deletedToken++;
						del++;
					}else if(op.type==LevenshteinDistance.INS){
						from[op.idx-1] += " /*<a>*/ " + op.token;
//						from[op.idx-1] += " " + op.token;
						addedToken++;
						add++;
					}else{
						from[op.idx] = "/*<d> " + from[op.idx] + "*/ /*<a>*/ " + op.token;
//						from[op.idx] = op.token;
						deletedToken++;
						addedToken++;
						add++;
						del++;
					}
				}
				if(ld.getDist()>0)
					from[0] = "\n/*<some tokens have been changed. add:"+add+", del:"+del+">*/\n"+from[0];
				int idt = 1;
				sb.append(indent(idt));
				for(int j=0; j<from.length; j++){
					sb.append(from[j]);
					if(from[j].equals(";") || from[j].equals("{") || from[j].equals("}")){
						if(from[j].equals("{")){
							idt++;
						}else if(j<from.length-1 && from[j+1].equals("}")){
							idt--;
						}
						sb.append("\n");
						sb.append(indent(idt));
					}else if(from[j].length()>0) sb.append(" ");
				}
			}
			removeSpaces(sb);
			sb.append("\n");
			res.append(sb.toString());
		}
		return res.toString();
	}
	
	StringBuilder tab = new StringBuilder();
	private String indent(int depth){
		tab.setLength(0);
		for(int i=0; i<depth; i++){
			tab.append('\t');
		}
		return tab.toString();
	}
	
	private void removeSpaces(StringBuilder sb){
		int i;
		for(i=sb.indexOf(" "); i!=-1 && i<sb.length()-1; i=sb.indexOf(" ", i+1)){
			if(i==0) continue;
			if(!isAlphabet(sb.charAt(i-1)) || !isAlphabet(sb.charAt(i+1))){
				sb.deleteCharAt(i);
			}
		}
	}
	
	private boolean isAlphabet(char c){
		return Character.isLowerCase(c) || Character.isUpperCase(c) || Character.isDigit(c) || c=='_';
	}
	
	private String format(String src){
		CodeFormatter codeFormatter = ToolFactory.createCodeFormatter(null);
		TextEdit textEdit = codeFormatter.format(CodeFormatter.K_COMPILATION_UNIT, src, 0, src.length(), 0, null);
		IDocument doc = new Document(src);
		try {
			textEdit.apply(doc);
			src = doc.get();
			return src;
		} catch (MalformedTreeException e) {
			e.printStackTrace();
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return "Format error.";
	}
	
	/*
	public void removeMethod(IMethod method){
		pushEnterRobot();
		try {
			RefactoringExecutionStarter.startDeleteRefactoring(new IMember[]{method}, getShell());
		} catch (JavaModelException e) {
			e.printStackTrace();
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	 */
	public void removeMethod(IMethod method){
		ICompilationUnit cu = method.getCompilationUnit();
		try {
			ASTParser parser = ASTParser.newParser(AST.JLS8);
			parser.setSource(cu);
			String src = cu.getSource();
			ASTNode node = parser.createAST(new NullProgressMonitor());
			CompilationUnit unit = ((CompilationUnit)node);
			unit.recordModifications();
			//		MethodFinder mf = new MethodFinder();
			Deleter pr = new Deleter(method);
			node.accept(pr);
			IDocument doc = new Document(src);
			TextEdit edit = unit.rewrite(doc, null);
			edit.apply(doc);
			src = doc.get();
			
			File file = new File(Data.gitDir+"/"+cu.getPath().toFile().getName());
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
			out.print(src);
			out.close();
		} catch (JavaModelException e) {
			e.printStackTrace();
		} catch (MalformedTreeException e) {
			e.printStackTrace();
		} catch (BadLocationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	class Deleter extends ASTVisitor{
		IMethod method;
		public Deleter(IMethod m) {
			method = m;
		}
		@Override
		public boolean visit(MethodDeclaration node) {
			if(equals(node)) node.delete();
			return false;
		}
		private boolean equals(MethodDeclaration node){
			if(!node.getName().getIdentifier().equals(method.getElementName())) return false;
			String[] p1 = method.getParameterTypes();
			List<SingleVariableDeclaration> list = node.parameters();
			if(p1.length != list.size()) return false;
			for(int i=0; i<p1.length; i++){
				if(!list.get(i).getType().toString()
						.equals(Signature.getSimpleName(Signature.getSignatureSimpleName(p1[i]))))
					return false;
			}
			return true;
		}
	}
	
	/*
	@SuppressWarnings("unused")
	@Deprecated
	private void removeMethod(ICompilationUnit cu, String methodname, Shell shell){
		pushEnterRobot();
		try {
			IMethod method = getIMethod(cu, methodname);
			RefactoringExecutionStarter.startDeleteRefactoring(new IMember[]{method}, shell);
		} catch (JavaModelException e) {
			e.printStackTrace();
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	*/
	public void pullUpFieldRefactoring(IField field, IType parent){
		PullUpRefactoringProcessor proc = new PullUpRefactoringProcessor
				(new IMember[]{field}, JavaPreferencesSettings.getCodeGenerationSettings(field.getJavaProject()));
		Refactoring ref = new ProcessorBasedRefactoring(proc);
		proc.setDestinationType(parent);
		proc.setMembersToMove(new IMember[]{field});
		refactoring(ref);
		/*
		pushEnterRobot();
		try {
			RefactoringExecutionStarter.startPullUpRefactoring(new IMember[]{field}, getShell());
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		*/
	}
	
	public void pullUpMethodRefactoring(IMethod method, IType parent){
		PullUpRefactoringProcessor proc = new PullUpRefactoringProcessor
				(new IMember[]{method}, JavaPreferencesSettings.getCodeGenerationSettings(method.getJavaProject()));
		Refactoring ref = new ProcessorBasedRefactoring(proc);
		proc.setDestinationType(parent);
		proc.setDeletedMethods(new IMethod[]{method});
		refactoring(ref);
//		pushEnterRobot();
//		try {
//			RefactoringExecutionStarter.startPullUpRefactoring(new IMember[]{method}, getShell());
//		} catch (JavaModelException e) {
//			e.printStackTrace();
//		}
	}
/*	
	@SuppressWarnings("unused")
	@Deprecated
	private void pullUpMethodRefactoring(ICompilationUnit cu, String methodname, Shell shell){
		pushEnterRobot();
		try {
			IMethod method = getIMethod(cu, methodname);
			RefactoringExecutionStarter.startPullUpRefactoring(new IMember[]{method}, shell);
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
	}
	*/
	private void pushEnterRobot(){
		
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
		        try {
		        	Thread.sleep(700);
					final Robot robot = new Robot();
			        robot.keyPress(KeyEvent.VK_ENTER);
			        robot.keyRelease(KeyEvent.VK_ENTER);
			        System.out.println("Pushed");
				} catch (AWTException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		thread.start();
		
/*
        try {
        	Thread.sleep(700);
			final Robot robot = new Robot();
	        robot.keyPress(KeyEvent.VK_ENTER);
	        robot.keyRelease(KeyEvent.VK_ENTER);
	        System.out.println("Pushed");
		} catch (AWTException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		*/
	}
	
	public void extractMethodRefactoring(ICompilationUnit cu, int s, int l, String method, int vis){
		ExtractMethodRefactoring ref = new ExtractMethodRefactoring(cu, s, l);
		ref.setMethodName(method);
		ref.setVisibility(vis);
		refactoring(ref);
	}
	
	public void extractMethodRefactoring(ICompilationUnit cu, int s, int t, MyMethod method){
		ExtractMethodRefactoring ref = new ExtractMethodRefactoring(cu, s, t);
		ref.setMethodName(method.name);
		ref.setVisibility(method.getVisiblity());
		refactoring(ref);
	}
	
	public void extractMethodRefactoring(ICompilationUnit cu, int s, int t, IMethod method){
		try{
			ExtractMethodRefactoring ref = new ExtractMethodRefactoring(cu, s, t);
			ref.setMethodName(method.getElementName());
			ref.setVisibility(method.getFlags());
			refactoring(ref);
		}catch(JavaModelException e){
			e.printStackTrace();
		}
	}
	
	private void refactoring(Refactoring refactoring){
		/* Refactoringインスタンスを用いてCheckConditionsOparationインスタンスを生成 */
		CheckConditionsOperation checkOP
		= new CheckConditionsOperation(refactoring, CheckConditionsOperation.ALL_CONDITIONS);
		
		/* 条件チェックを実行 */
		try {
			checkOP.run(new NullProgressMonitor());
			
			/* Refactoringインスタンスを用いてCreateChangeOparationインスタンスを生成 */
			CreateChangeOperation changeOP = new CreateChangeOperation(refactoring);

			/* Changeの生成を実行 */
			changeOP.run(new NullProgressMonitor());

			/* 生成されたChangeインスタンスを取得 */
			Change change = changeOP.getChange();

			/* Changeインスタンスを用いてPerformChangeOperationインスタンスを生成 */
			PerformChangeOperation op = new PerformChangeOperation(change);

			/* ソースコードにChangeを適用する */
			op.run(new NullProgressMonitor());

		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	private Shell getShell(){
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	}
}
