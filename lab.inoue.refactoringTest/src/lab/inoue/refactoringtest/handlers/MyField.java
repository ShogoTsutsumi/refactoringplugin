package lab.inoue.refactoringtest.handlers;

public class MyField extends MyMember{
	public MyField(int scope, String type, String name) {
		super(scope, type, name);
	}
	@Override
	void setHash() {
		hash = name.hashCode();
	}
	@Override
	public int hashCode() {
		return hash;
	}
	@Override
	public boolean equals(Object o) {
		return name.equals(((MyField)o).name);
	}
	@Override
	public String toString() {
		return TYPES[scope]+type+" "+name;
	}
}
