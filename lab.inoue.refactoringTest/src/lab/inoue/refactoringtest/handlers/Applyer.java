package lab.inoue.refactoringtest.handlers;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;

public interface Applyer {
	public void apply();
}

class RemoveApplyer implements Applyer{
	IMethod method;
	RefApplyer ra;
	public RemoveApplyer(IMethod m, RefApplyer ra) {
		method = m;
		this.ra = ra;
	}
	@Override
	public void apply() {
		ra.removeMethod(method);
	}
	@Override
	public String toString() {
		return "Rem-"+method.toString();
	}
}

class PullUpMethodApplyer implements Applyer{
	IMethod method;
	IType parent;
	RefApplyer ra;
	public PullUpMethodApplyer(IMethod m, IType parent, RefApplyer ra) {
		this.method = m;
		this.ra = ra;
		this.parent = parent;
	}
	@Override
	public void apply() {
		ra.pullUpMethodRefactoring(method, parent);
	}
	@Override
	public String toString() {
		return "PU-"+method.toString();
	}
}

class DeleteAndPullUpMethodApplyer implements Applyer{
	IMethod method;
	IMethod parent;
	RefApplyer ra;
	public DeleteAndPullUpMethodApplyer(IMethod m, IMethod parent, RefApplyer ra) {
		this.method = m;
		this.ra = ra;
		this.parent = parent;
	}
	@Override
	public void apply() {
		ra.removeMethod(parent);
		ra.refresh();
		ra.pullUpMethodRefactoring(method, parent.getDeclaringType());
	}
	@Override
	public String toString() {
		return "DP-"+method.toString()+","+parent.toString();
	}
}

class PullUpFieldApplyer implements Applyer{
	IField field;
	IType par;
	RefApplyer ra;
	public PullUpFieldApplyer(IField f, IType parent, RefApplyer ra) {
		field = f;
		par = parent;
		this.ra = ra;
	}
	
	@Override
	public void apply() {
		ra.pullUpFieldRefactoring(field, par);
	}
	@Override
	public String toString() {
		return "PU-"+field.toString();
	}
}

class ExtractMethodApplyer implements Applyer{
	IMethod createdMethod;
	ICompilationUnit oldCU;
	int start, len;
	RefApplyer ra;
	/**
	 * @param cu 旧ソースコードのICompilationUnit
	 * @param s 抜き出しの開始点
	 * @param l 抜き出し終了点+1
	 * @param method 新版で抽出されたメソッド
	 */
	public ExtractMethodApplyer(ICompilationUnit cu, int s, int e, IMethod method, RefApplyer ra) {
		createdMethod = method;
		oldCU = cu;
		start = s;
		len = e-s;
		this.ra = ra;
	}
	@Override
	public void apply() {
		ra.extractMethodRefactoring(oldCU, start, len, createdMethod);
	}
	@Override
	public String toString() {
		return "EM-"+createdMethod.getElementName();
	}
}