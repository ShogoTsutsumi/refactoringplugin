package lab.inoue.refactoringtest.handlers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;

/**
 * the command has been executed, so extract extract the needed information
 * from the application context.
 */
class MyMethod extends MyMember{
	String pstr;
	String[] body;
	List parameter;
	MyMethod(int scope, String type, String name, List par, String[] body){
		super(scope, type, name);
		this.parameter = par;
		pstr = par.toString();
		this.body = body;
		setHash();
	}
	@Override
	void setHash() {
		hash = name.hashCode() ^ pstr.hashCode();
	}
	int getVisiblity(){
		return scope;
	}
	@Override
	public String toString() {
		return TYPES[scope]+type+" "+name+pstr;
	}
	@Override
	public int hashCode() {
		return hash;
	}
	@Override
	public boolean equals(Object obj) {
		MyMethod m = (MyMethod)obj;
		return name.equals(m.name) && pstr.equals(m.pstr);
//		if(name.equals(m.name) && parameter.size()==m.parameter.size()){
//			for(int i=0; i<parameter.size(); i++){
//				if(!parameter.get(i).toString().equals(m.parameter.get(i).toString()))
//					return false;
//			}
//			return true;
//		}
//		return false;
	}
}