package lab.inoue.refactoringtest.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

public class FieldFinder extends ASTVisitor{
	private List<MyField> fields;
	public FieldFinder() {
		fields = new ArrayList<>();
	}
	public List<MyField> getFields(){
		return fields;
	}
	@Override
	public void preVisit(ASTNode node) {
		if(node.getNodeType()==ASTNode.FIELD_DECLARATION){
			FieldDeclaration field = (FieldDeclaration)node;
			
			int modifire = field.getModifiers();
			String type = field.getType().toString();
			for(Object o: field.fragments()){
				if(!(o instanceof VariableDeclarationFragment)) continue;
				VariableDeclarationFragment vd = (VariableDeclarationFragment)o;
				String name = vd.getName().getIdentifier();
				fields.add(new MyField(modifire, type, name));
			}
//			System.out.printf("可視性等\t=%s\n", method.modifiers());
//			System.out.printf("戻り型\t=%s\n", method.getReturnType2());
//			System.out.printf("メソッド名\t=%s\n", method.getName().getIdentifier());
//			System.out.printf("引数 \t=%s\n", method.parameters());
		}
	}
}

class OneFieldFinder extends ASTVisitor{
	private MyField oldField;
	private MyField found;
	public OneFieldFinder(MyField m){
		oldField = m;
	}
	public MyField getMethod(){
		return found;
	}
	@Override
	public void preVisit(ASTNode node) {
		if(found != null) return;
		if(node.getNodeType()==ASTNode.VARIABLE_DECLARATION_FRAGMENT){
			FieldDeclaration field = (FieldDeclaration)node;
			int modifire = field.getModifiers();
			String type = field.getType().toString();
			for(Object o: field.fragments()){
				if(!(o instanceof VariableDeclarationFragment)) continue;
				VariableDeclarationFragment vd = (VariableDeclarationFragment)o;
				String name = vd.getName().getIdentifier();
				MyField f = new MyField(modifire, type, name);
				if(f.equals(oldField)){
					found = f;
					break;
				}
			}
		}
	}
}
