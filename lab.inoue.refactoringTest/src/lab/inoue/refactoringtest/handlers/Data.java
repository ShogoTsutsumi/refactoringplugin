package lab.inoue.refactoringtest.handlers;

public class Data {
	/*
	// execute ant
	// work on lab computer
	final static String oldProject = "Ant_old";		// old project name on workspace
	final static String newProject = "Ant_new";		// new project name on workspace
	final static String className = "ComponentHelper";			// refactored class name
	final static String packageName = "main.org.apache.tools.ant";	// package name where class "className" exist
	final static String workSpacePath = "C:/Users/threepipes_s/workspace";
	final static String testDir = "C:/Work/lab/ant/ant_old/ant";
	final static String gitDir = "C:/Work/lab/ant/ant_old/ant/src/"+packageName.replace('.', '/');
	final static String OutputDir = "C:/Work/lab/Result";
	final static String[] addFiles = { // these files will be added to git (what be changed must be included)
		className+".java",
//		"BasicParserConfiguration.java"
	};
	*/
	
	// execute ant
	// work on laptop
	final static String oldProject = "Ant_old";		// old project name on workspace
	final static String newProject = "Ant_new";		// new project name on workspace
	final static String className = "Launcher";			// refactored class name
	final static String packageName = "main.org.apache.tools.ant.launch";	// package name where class "className" exist
	final static String workSpacePath = "C:/Users/threepipes_s/workspace";
	final static String testDir = "C:/Users/threepipes_s/Work/laboratory/Downloads/ant";
	final static String gitDir = "C:/Users/threepipes_s/Work/laboratory/Downloads/ant/src/"+packageName.replace('.', '/');
	final static String OutputDir = "C:/Users/threepipes_s/Work/laboratory/Result";
	final static String[] addFiles = { // these files will be added to git (what be changed must be included)
		className+".java",
//		"BasicParserConfiguration.java"
	};/**/
	
	/*
	final static String className = "DOMElementWriter";			// refactored class name
	final static String packageName = "main.org.apache.tools.ant.util";	// package name where class "className" exist
	 */
	/*
	// work on lab computer
	final static String oldProject = "XercesForPlugin";		// old project name on workspace
	final static String newProject = "XercesForPlugin2";	// new project name on workspace
	final static String className = "XIncludeHandler";			// refactored class name
	final static String packageName = "org.apache.xerces.xinclude";	// package name where class "className" exist
	final static String workSpacePath = "C:/Users/threepipes_s/workspace";
	final static String testDir = "C:/Work/lab/xerces_old/trunk";
	final static String gitDir = "C:/Work/lab/xerces_old/trunk/src/"+packageName.replace('.', '/');
	final static String OutputDir = "C:/Work/lab/Result";
	final static String[] addFiles = { // these files will be added to git (what be changed must be included)
		className+".java",
//		"BasicParserConfiguration.java"
	};*/
	/*
	// work on laptop
	final static String oldProject = "Xerces";
	final static String newProject = "Xerces_2";
	final static String className = "DocumentImpl";
	final static String packageName = "org.apache.xerces.dom";
	final static String workSpacePath = "C:/Users/threepipes_s/workspace";
	final static String testDir = "C:/Users/threepipes_s/Work/laboratory/Downloads/xerces_old/trunk";
	final static String gitDir = "C:/Users/threepipes_s/Work/laboratory/Downloads/xerces_old/trunk/src/org/apache/xerces/dom";
	final static String OutputDir = "C:/Users/threepipes_s/Work/laboratory/Result";
	final static String[] addFiles = {
			className+".java",
//			"DocumentImpl.java",
//			"CoreDocumentImpl.java"
	};/**/
}
