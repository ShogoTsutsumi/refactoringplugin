package lab.inoue.refactoringtest.handlers;

public abstract class MyMember {
	String name;
	String type;
	int scope;
	int hash;
	final static int NONE = 0;
	final static int PUBLIC = 1;
	final static int PRIVATE = 2;
	final static int PROTECTED = 4;
	final String[] TYPES = {
			"", "public ", "private ",null, "protected "
	};
	MyMember(int scope, String type, String name){
		this.name = name;
		this.type = type;
		this.scope = scope;
	}
	abstract void setHash();
}
