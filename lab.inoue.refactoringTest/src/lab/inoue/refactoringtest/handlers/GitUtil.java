package lab.inoue.refactoringtest.handlers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

public class GitUtil {
	static String dir = Data.gitDir;
	IProject prj;
	public GitUtil(String[] addFilename, IProject p) {
		initDir();
		execProcess(new String[]{"git", "init"}, dir);
		String[] cmd = new String[2+addFilename.length];
		cmd[0] = "git";
		cmd[1] = "add";
		for(int i=0; i<addFilename.length; i++){
			cmd[2+i] = addFilename[i];
		}
		execProcess(cmd, dir);
		prj = p;
	}
	
	private void initDir(){
		deleteDir(new File(dir+"/.git"));
	}
	
	private void deleteDir(File f){
		if(f.exists()) f.delete();
	}
	
	final static String testDir = "C:/Users/threepipes_s/workspace/TestGit/src";
	public GitUtil(IProject prj) {
		this.prj = prj;
		dir = testDir;
		(new File(dir)).mkdir();
		String out = execProcess(new String[]{"git", "init"}, dir);
		System.out.println(out);
		out = execProcess(new String[]{"git", "add", "."}, dir);
		System.out.println(out);
	}
	
	public void reset(){
		execProcess(new String[]{"git", "reset", "--hard"}, dir);
	}
	
	public String commit(String comment){
		if(comment==null) comment = "No comment";
//		execProcess(new String[]{"git", "add", "."}, dir);
		String out = execProcess(new String[]{"git", "commit", "-a", "-m", "\""+comment+"\""}, dir);
		System.out.println(out);
		if(out.charAt(0)!='[') return null; // 変化のないコミット
		
		String[] l1 = out.split("\n")[0].split("]")[0].split(" ");
		return l1[l1.length-1];
	}
	
	public void checkout(String branch){
		execProcess(new String[]{"git", "checkout", branch}, dir);
		try {
			prj.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}
	
	public void checkout(String branch, String key){
		execProcess(new String[]{"git", "checkout", branch}, dir);
		if(key != null) execProcess(new String[]{"git", "checkout", key}, dir);
		try {
			prj.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}
	
	private void createBranch(String name){
		execProcess(new String[]{"git", "checkout", "-b", name}, dir);
	}
	
	public String branchAndCommit(String branch, String comment){
		createBranch(branch);
		return commit(comment);
	}
	
	private String execProcess(String[] cmd, String dir){
		try {
			ProcessBuilder pb = new ProcessBuilder(cmd);
			if(dir != null) pb.directory(new File(dir));
			Process pc;
			pc = pb.start();
			BufferedReader br = new BufferedReader(
					new InputStreamReader(pc.getInputStream()));
			String line;
			StringBuilder sb = new StringBuilder();
			while((line = br.readLine()) != null){
				sb.append(line);
			}
			pc.waitFor();
			br.close();
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
}
